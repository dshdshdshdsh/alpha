package com.company.alphabank;

import java.util.*;
import java.lang.*;
import java.io.*;

class Rextester
{
    public static void main(String args[]) throws IOException
    {
        System.out.println("Вычисление факториала:");
        Factorial();

        int[] arr = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
        System.out.println("Начальные данные в массиве:");
        PrintArray(arr);
        System.out.println();

        System.out.println("Случайно замешиваем массив:");
        arr=RandomArray(arr, 100);
        PrintArray(arr);
        System.out.println();

        System.out.println("Запись массива в файл arr.txt.");
        WriteArray(arr);

        System.out.println("Чтение массива из файла arr.txt:");
        arr=ReadArray();
        PrintArray(arr);
        System.out.println();

        System.out.println("Упорядочивание массива по возрастанию:");
        arr=ArraySortBy(true, arr);
        PrintArray(arr);
        System.out.println();

        System.out.println("Упорядочивание массива по убыванию:");
        arr=ArraySortBy(false, arr);
        PrintArray(arr);
        System.out.println();

    }
    public static void Factorial()
    {
        System.out.println(calculateFactorial(20));
    }

    public static long calculateFactorial(int n)
    {
        long result = 1;

        for (int i = 1; i <=n; i ++)
            result = result*i;

        return result;
    }
    public static int[] ArraySortBy(boolean inc, int[] arr)
    {
        int i = 0;
        boolean stop = false;
        while (!stop) {
            stop = true;
            i = 0;
            while (i < arr.length - 1) {
                if (inc) {
                    if (arr[i] > arr[i + 1]) {
                        arr = ArrayReplacer(arr, i);
                        stop =false;
                    }
                } else {
                    if (arr[i] < arr[i + 1]) {
                        arr = ArrayReplacer(arr, i);
                        stop = false;
                    }
                }
                i++;
            }
        }

        return arr;
    }
    public static int[] ArrayReplacer(int[] arr, int i)
    {
        int tmp = arr[i];
        arr[i] = arr[i + 1];
        arr[i + 1] = tmp;
        return arr;
    }
    public static int[] RandomArray(int[] arr, int n)
    {
        Random random = new Random();
        int j1 = 0, j2 = 0, tmp = 0;
        for (int i = 0; i < n; i++)
        {
            j1 = random.nextInt(21);
            j2 = random.nextInt(21);

            tmp = arr[j1];
            arr[j1] = arr[j2];
            arr[j2] = tmp;
        }

        return arr;
    }
    public static void PrintArray(int[] arr)
    {
        for (int i : arr)
            System.out.printf("%d ", i);
    }
    public static int[] ReadArray() throws IOException
    {
        FileReader reader = new FileReader("arr.txt");
        BufferedReader breader = new BufferedReader(reader);

        String result = breader.readLine();
        String[] arrString = result.split(",");
        int[] ret = new int[arrString.length];
        for (int i = 0; i < arrString.length; i++)
            ret[i] = Integer.valueOf(arrString[i]);

        breader.close();
        reader.close();

        return ret;
    }
    public static void WriteArray(int[] arr) throws IOException
    {
        FileWriter writer = new FileWriter("arr.txt", false);

            for (int i = 0; i < arr.length; i++)
            {
                if (i > 0)
                    writer.write(',');
                writer.write(String.valueOf(arr[i]));
            }

            writer.close();
    }
}